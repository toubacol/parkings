package com.instantsystem.exercice;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.instantsystem.exercice.controller.ParkingController;


/**
 * Unit test for ParkingApp.
 * @author fdiop
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ParkingAppTest {
    @Autowired
    private ParkingController parkingController;

    @Test
    public void contexLoads() throws Exception {
	assertNotNull(parkingController);
    }
}
