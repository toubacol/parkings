package com.instantsystem.exercice.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.instantsystem.exercice.model.Parking;


/**
 * @author fdiop
 * ParkingController end to end test class
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ParkingControllerEndToEnd {

    @Autowired
    MockMvc mvc;
    
    @Test
    public void getParkings_Default_test() throws Exception {
	Gson gson = new Gson();
	MvcResult result = mvc 
                .perform(get("/parkings"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
	
	MockHttpServletResponse response = result.getResponse();
	assertNotNull(response);
	String bodyString = response.getContentAsString();
	assertNotNull(bodyString);
	List<Parking> listParking = gson.fromJson(bodyString, new TypeToken<ArrayList<Parking>>(){}.getType());
	assertNotNull(listParking);
	assertFalse(listParking.isEmpty());
	
	// test the getById
	Long parkingId = listParking.get(0).getId();
	MvcResult resultGetById = mvc 
                .perform(get("/parkings/" + parkingId))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
	
	MockHttpServletResponse responseGetById = resultGetById.getResponse();
	assertNotNull(responseGetById);
	String bodyStringGetById = responseGetById.getContentAsString();
	assertNotNull(bodyStringGetById);
	Parking parking = gson.fromJson(bodyStringGetById, Parking.class);
	assertNotNull(parking);
	assertEquals(listParking.get(0).getId(), parking.getId());
    }
    
    @Test
    public void getParkingById_FakeId_test() throws Exception {
	 mvc.perform(get("/parkings/" + "fake_Id"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andReturn();
    }
}
