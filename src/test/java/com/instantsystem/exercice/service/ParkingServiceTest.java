package com.instantsystem.exercice.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyString;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import com.instantsystem.exercice.exception.ParkingException;
import com.instantsystem.exercice.exception.ParkingNotFoundException;
import com.instantsystem.exercice.model.Parking;

/**
 * @author fdiop
 * ParkingService test class
 */
@SpringBootTest
public class ParkingServiceTest {

    private static final String MOCK_PARKINGS_FILE_PATH = "src/test/resources/parkings.json";
    private static final int START = 2;
    private static final String RENNES = "Rennes";
    private static final int ROWS = 20;
    private static final double DIST = 1500D;
    private static final double LON = -1.7234738D;
    private static final double LAT = 48.1159102D;
    private static final String MOCK_PARKING_BY_ID_FILE_PATH = "src/test/resources/parkingById.json";
    private static final String MOCK_PARKING_BY_ID_NOT_FOUND_FILE_PATH = "src/test/resources/parkingById_NotFound.json";
    
    @InjectMocks
    private ParkingService parkingService;
    @Mock
    private ParkingRestClient parkingRestClient;
    
    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(parkingService, "recordsPropertydName", "records");
        ReflectionTestUtils.setField(parkingService, "parkingModelPropertydName", "fields");
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void getParkingList_Success_Test() throws IOException, ParseException {
	JSONParser parser = new JSONParser();
	String bodyString = IOUtils.toString(new FileSystemResource(MOCK_PARKINGS_FILE_PATH).getInputStream(), Charset.defaultCharset());
	JSONObject mockedBody = (JSONObject) parser.parse(bodyString);
	ResponseEntity mockedResponseOk = new ResponseEntity(mockedBody, HttpStatus.OK);
	when(this.parkingRestClient.getParkingsRest(LAT, LON, DIST, ROWS, START, RENNES, null)).thenReturn(mockedResponseOk);
	
	 List<Parking> parkingsList = parkingService.getParkingList(LAT, LON, DIST, ROWS, START, RENNES, null);
	assertNotNull(parkingsList);
	assertEquals(3, parkingsList.size());
    }
    
    @Test
    public void getParkingList_Null_Test() {
	when(this.parkingRestClient.getParkingsRest(LAT, LON, DIST, ROWS, START, RENNES, null)).thenReturn(null);
	
	 List<Parking> parkingsList = parkingService.getParkingList(LAT, LON, DIST, ROWS, START, RENNES, null);
	 assertNotNull(parkingsList);
	 assertTrue(parkingsList.isEmpty());
    }
    
    @Test
    public void mapToParkingModel_Null_Test()  {
	JSONObject responseBody = null;
	List<Parking> parkingsList = ReflectionTestUtils.invokeMethod(parkingService,"mapToParkingModel", responseBody );
	assertNotNull(parkingsList);
	assertTrue(parkingsList.isEmpty());
    }
    
    @SuppressWarnings("unchecked")
    @Test(expected=ParkingException.class)
    public void mapToParkingModel_ParseException_Test() throws ParseException  {
	JSONObject responseBody = new JSONObject();
	JSONArray records = new JSONArray();
	responseBody.put("recordsPropertydName", records);
	
	JSONParser parser = Mockito.mock(JSONParser.class);
	when(parser.parse(anyString())).thenThrow(ParseException.class);
	
	ReflectionTestUtils.setField(parkingService, "parser", parser);
	ReflectionTestUtils.invokeMethod(parkingService,"mapToParkingModel", responseBody );
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void getParkingById_Success_Test() throws IOException, ParseException {
	JSONParser parser = new JSONParser();
	String bodyString = IOUtils.toString(new FileSystemResource(MOCK_PARKING_BY_ID_FILE_PATH).getInputStream(), Charset.defaultCharset());
	JSONObject mockedBody = (JSONObject) parser.parse(bodyString);
	ResponseEntity mockedResponseOk = new ResponseEntity(mockedBody, HttpStatus.OK);
	when(this.parkingRestClient.getParkingsRest(null, null, null, null, null, null, "parkingId")).thenReturn(mockedResponseOk);
	
	 Parking parking = parkingService.getParkingById("parkingId");
	assertNotNull(parking);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test(expected=ParkingNotFoundException.class)
    public void getParkingById_NotFound_Test() throws IOException, ParseException {
	JSONParser parser = new JSONParser();
	String bodyString = IOUtils.toString(new FileSystemResource(MOCK_PARKING_BY_ID_NOT_FOUND_FILE_PATH).getInputStream(), Charset.defaultCharset());
	JSONObject mockedBody = (JSONObject) parser.parse(bodyString);
	ResponseEntity mockedResponseOk = new ResponseEntity(mockedBody, HttpStatus.OK);
	when(this.parkingRestClient.getParkingsRest(null, null, null, null, null, null, "parkingId")).thenReturn(mockedResponseOk);
	
	parkingService.getParkingById("parkingId");
    }
}
