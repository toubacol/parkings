package com.instantsystem.exercice.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.instantsystem.exercice.exception.InvalidCoordinateException;
import com.instantsystem.exercice.exception.InvalidDistanceException;
import com.instantsystem.exercice.exception.ParkingException;

/**
 * @author fdiop
 * ParkingRestClient test class
 */
@SpringBootTest
public class ParkingRestClientTest {

    private static final String MOCK_PARKINGS_FILE_PATH = "src/test/resources/parkings.json";
    private static final int START = 2;
    private static final String RENNES = "Rennes";
    private static final int ROWS = 20;
    private static final double DIST = 1500D;
    private static final double LON = -1.7234738D;
    private static final double LAT = 48.1159102D;
    @InjectMocks
    private ParkingRestClient parkingRestClient;
    @Mock
    private RestTemplate restTemplate;
    
    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(parkingRestClient, "parkingsApiUrl", "http://test/parkingsApiUrl/?dataset=test");
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void getParkingsRest_Success_Test() throws IOException {
	ResponseEntity mockedResponseOk = new ResponseEntity(IOUtils.toString(new FileSystemResource(MOCK_PARKINGS_FILE_PATH).getInputStream(), Charset.defaultCharset()), HttpStatus.OK);
	String builtUrl = "http://test/parkingsApiUrl/?dataset=test&geofilter.distance=48.1159102,-1.7234738,1500.0&rows=20&start=2&q=Rennes";
	when(this.restTemplate.exchange(builtUrl, HttpMethod.GET, HttpEntity.EMPTY, JSONObject.class)).thenReturn(mockedResponseOk);
	
	ResponseEntity<JSONObject> response = parkingRestClient.getParkingsRest(LAT, LON, DIST, ROWS, START, RENNES, null);
	assertNotNull(response);
	assertNotNull(response.getBody());
	assertEquals(mockedResponseOk, response);
    }
    
    @Test(expected=ParkingException.class)
    public void getParkingsRest_HttpClientErrorException_Test() {
	String builtUrl = "http://test/parkingsApiUrl/?dataset=test&geofilter.distance=48.1159102,-1.7234738,1500.0&rows=20&start=2&q=Rennes";
	when(this.restTemplate.exchange(builtUrl, HttpMethod.GET, HttpEntity.EMPTY, JSONObject.class)).thenThrow(HttpClientErrorException.class);
	parkingRestClient.getParkingsRest(LAT, LON, DIST, ROWS, START, RENNES, null);
    }
    
    @Test(expected=ParkingException.class)
    public void getParkingsRest_RestClientException_Test() {
	String builtUrl = "http://test/parkingsApiUrl/?dataset=test&geofilter.distance=48.1159102,-1.7234738,1500.0&rows=20&start=2&q=Rennes";
	when(this.restTemplate.exchange(builtUrl, HttpMethod.GET, HttpEntity.EMPTY, JSONObject.class)).thenThrow(RestClientException.class);
	parkingRestClient.getParkingsRest(LAT, LON, DIST, ROWS, START, RENNES, null);
    }
    
    @Test
    public void buildAdditionalQueryParams_No_Param_Test() {
	String additionalQueryParams = ReflectionTestUtils.invokeMethod(parkingRestClient,"buildAdditionalQueryParams", null, null, null, null);
	assertNotNull(additionalQueryParams);
	assertTrue(additionalQueryParams.isEmpty());
    }
    
    @Test
    public void buildAdditionalQueryParams_Full_Params_Test() {
	String expected = "&rows=20&start=2&q=Rennes";
	String additionalQueryParams = ReflectionTestUtils.invokeMethod(parkingRestClient,"buildAdditionalQueryParams", ROWS, START, RENNES, null);
	assertNotNull(additionalQueryParams);
	assertEquals(expected, additionalQueryParams.toString());
    }
    
    @Test
    public void buildAdditionalQueryParams_Mixed_Params1_Test() {
	String expected = "&start=2&q=Rennes";
	String additionalQueryParams = ReflectionTestUtils.invokeMethod(parkingRestClient,"buildAdditionalQueryParams", null, START, RENNES, null);
	assertNotNull(additionalQueryParams);
	assertEquals(expected, additionalQueryParams.toString());
    }
    
    @Test
    public void buildAdditionalQueryParams_Mixed_Params2_Test() {
	String expected = "&rows=20&q=Rennes";
	String additionalQueryParams = ReflectionTestUtils.invokeMethod(parkingRestClient,"buildAdditionalQueryParams", ROWS, null, RENNES, null);
	assertNotNull(additionalQueryParams);
	assertEquals(expected, additionalQueryParams.toString());
    }
    
    @Test
    public void buildAdditionalQueryParams_Mixed_Params3_Test() {
	String expected = "&refine.id=parkingId";
	String additionalQueryParams = ReflectionTestUtils.invokeMethod(parkingRestClient,"buildAdditionalQueryParams", null, null, null, "parkingId");
	assertNotNull(additionalQueryParams);
	assertEquals(expected, additionalQueryParams.toString());
    }
    
    @Test
    public void validateSearchPointAndDist_Success_Test() {
	String expected = "&geofilter.distance=48.1159102,-1.7234738,1500.0";
	String searchQueryParams = ReflectionTestUtils.invokeMethod(parkingRestClient,"validateSearchPointAndDist", LAT, LON, DIST);
	assertNotNull(searchQueryParams);
	assertEquals(expected, searchQueryParams.toString());
    }
    
    @Test
    public void validateSearchPointAndDist_No_coordinates_Test() {
	String searchQueryParams = ReflectionTestUtils.invokeMethod(parkingRestClient,"validateSearchPointAndDist", null, null, null);
	assertNotNull(searchQueryParams);
	assertTrue(searchQueryParams.isEmpty());
    }
    
    @Test(expected=InvalidCoordinateException.class)
    public void validateSearchPointAndDist_Invalid_lat_Test() {
	ReflectionTestUtils.invokeMethod(parkingRestClient,"validateSearchPointAndDist", null, LON, null);
    }
    
    @Test(expected=InvalidCoordinateException.class)
    public void validateSearchPointAndDist_Invalid_lon_Test() {
	ReflectionTestUtils.invokeMethod(parkingRestClient,"validateSearchPointAndDist", LAT, null, null);
    }
    
    @Test(expected=InvalidCoordinateException.class)
    public void validateSearchPointAndDist_Distance_without_coordinates_Test() {
	ReflectionTestUtils.invokeMethod(parkingRestClient,"validateSearchPointAndDist", null, null, 1000D);
    }
    
    @Test(expected=InvalidDistanceException.class)
    public void validateSearchPointAndDist_Ivalid_dist_Null_Test() {
	ReflectionTestUtils.invokeMethod(parkingRestClient,"validateSearchPointAndDist", LAT, LON, null);
    }
    
    @Test(expected=InvalidDistanceException.class)
    public void validateSearchPointAndDist_Ivalid_dist_Negative_Test() {
	ReflectionTestUtils.invokeMethod(parkingRestClient,"validateSearchPointAndDist", LAT, LON, -5D);
    }
    
    @Test(expected=InvalidDistanceException.class)
    public void validateSearchPointAndDist_Ivalid_dist_Zero_Test() {
	ReflectionTestUtils.invokeMethod(parkingRestClient,"validateSearchPointAndDist", LAT, LON, 0D);
    }
}
