package com.instantsystem.exercice.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * POJO representing a Parking
 * @author fdiop
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Parking {

	private String status;
	private String tarif_1h30;
	private Double dist;
	private String tarif_30;
	private String tarif_1h;
	private Integer max;
	private String orgahoraires;
	private String tarif_3h;
	private String tarif_2h;
	private Integer free;
	private String key;
	private String tarif_4h;
	private double[] geo;
	private Long id;
	private String tarif_15;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTarif_1h30() {
		return tarif_1h30;
	}
	public void setTarif_1h30(String tarif_1h30) {
		this.tarif_1h30 = tarif_1h30;
	}
	public Double getDist() {
		return dist;
	}
	public void setDist(Double dist) {
		this.dist = dist;
	}
	public String getTarif_30() {
		return tarif_30;
	}
	public void setTarif_30(String tarif_30) {
		this.tarif_30 = tarif_30;
	}
	public String getTarif_1h() {
		return tarif_1h;
	}
	public void setTarif_1h(String tarif_1h) {
		this.tarif_1h = tarif_1h;
	}
	public Integer getMax() {
		return max;
	}
	public void setMax(Integer max) {
		this.max = max;
	}
	public String getOrgahoraires() {
		return orgahoraires;
	}
	public void setOrgahoraires(String orgahoraires) {
		this.orgahoraires = orgahoraires;
	}
	public String getTarif_3h() {
		return tarif_3h;
	}
	public void setTarif_3h(String tarif_3h) {
		this.tarif_3h = tarif_3h;
	}
	public String getTarif_2h() {
		return tarif_2h;
	}
	public void setTarif_2h(String tarif_2h) {
		this.tarif_2h = tarif_2h;
	}
	public Integer getFree() {
		return free;
	}
	public void setFree(Integer free) {
		this.free = free;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTarif_4h() {
		return tarif_4h;
	}
	public void setTarif_4h(String tarif_4h) {
		this.tarif_4h = tarif_4h;
	}
	public double[] getGeo() {
		return geo;
	}
	public void setGeo(double[] geo) {
		this.geo = geo;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTarif_15() {
		return tarif_15;
	}
	public void setTarif_15(String tarif_15) {
		this.tarif_15 = tarif_15;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dist == null) ? 0 : dist.hashCode());
		result = prime * result + ((free == null) ? 0 : free.hashCode());
		result = prime * result + Arrays.hashCode(geo);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((max == null) ? 0 : max.hashCode());
		result = prime * result + ((orgahoraires == null) ? 0 : orgahoraires.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tarif_15 == null) ? 0 : tarif_15.hashCode());
		result = prime * result + ((tarif_1h == null) ? 0 : tarif_1h.hashCode());
		result = prime * result + ((tarif_1h30 == null) ? 0 : tarif_1h30.hashCode());
		result = prime * result + ((tarif_2h == null) ? 0 : tarif_2h.hashCode());
		result = prime * result + ((tarif_30 == null) ? 0 : tarif_30.hashCode());
		result = prime * result + ((tarif_3h == null) ? 0 : tarif_3h.hashCode());
		result = prime * result + ((tarif_4h == null) ? 0 : tarif_4h.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parking other = (Parking) obj;
		if (dist == null) {
			if (other.dist != null)
				return false;
		} else if (!dist.equals(other.dist))
			return false;
		if (free == null) {
			if (other.free != null)
				return false;
		} else if (!free.equals(other.free))
			return false;
		if (!Arrays.equals(geo, other.geo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (max == null) {
			if (other.max != null)
				return false;
		} else if (!max.equals(other.max))
			return false;
		if (orgahoraires == null) {
			if (other.orgahoraires != null)
				return false;
		} else if (!orgahoraires.equals(other.orgahoraires))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (tarif_15 == null) {
			if (other.tarif_15 != null)
				return false;
		} else if (!tarif_15.equals(other.tarif_15))
			return false;
		if (tarif_1h == null) {
			if (other.tarif_1h != null)
				return false;
		} else if (!tarif_1h.equals(other.tarif_1h))
			return false;
		if (tarif_1h30 == null) {
			if (other.tarif_1h30 != null)
				return false;
		} else if (!tarif_1h30.equals(other.tarif_1h30))
			return false;
		if (tarif_2h == null) {
			if (other.tarif_2h != null)
				return false;
		} else if (!tarif_2h.equals(other.tarif_2h))
			return false;
		if (tarif_30 == null) {
			if (other.tarif_30 != null)
				return false;
		} else if (!tarif_30.equals(other.tarif_30))
			return false;
		if (tarif_3h == null) {
			if (other.tarif_3h != null)
				return false;
		} else if (!tarif_3h.equals(other.tarif_3h))
			return false;
		if (tarif_4h == null) {
			if (other.tarif_4h != null)
				return false;
		} else if (!tarif_4h.equals(other.tarif_4h))
			return false;
		return true;
	}
	
	
}
