package com.instantsystem.exercice.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.instantsystem.exercice.model.Parking;
import com.instantsystem.exercice.service.ParkingService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


/**
 * Rest controller for Parking resources
 * @author fdiop
 *
 */
@RestController
public class ParkingController {

	private static Logger logger = LoggerFactory.getLogger(ParkingController.class);
	
	@Autowired
	private ParkingService parkingService;
	
    @ApiOperation(value = "Get the list of parkings around the given coordinates")
    @ApiResponses(value = { 
	    @ApiResponse(code = 400, message = "Invalid request"),
	    @ApiResponse(code = 500, message = "Internal Server Error"),
	    @ApiResponse(code = 200, message = "List of parkings ") })
	@RequestMapping(value="/parkings", method=RequestMethod.GET)
    public List<Parking>  getParkings (
	    	@ApiParam(name = "lat", value = "latitude") @RequestParam(value="lat", required=false) Double lat,
	    	@ApiParam(name = "lon", value = "longitude") @RequestParam(value="lon", required=false) Double lon,
	    	@ApiParam(name = "dist", value = "distance around the given coordinates in meters") @RequestParam(value="dist", required=false) Double dist, 
	    	@ApiParam(name = "rows", value = "total rows to display") @RequestParam(value="rows", required=false) Integer rows,
	    	@ApiParam(name = "start", value = "starting index") @RequestParam(value="start", required=false) Integer start,
	    	@ApiParam(name = "search", value = "full text search") @RequestParam(value="search", required=false) String fullTextSearch) {
        
		logger.debug("GET parkings called with lat={}, lon={}, dist={}, rows={}, start={}, search={}", 
			lat, lon, dist, rows, start, fullTextSearch);
		 
		return parkingService.getParkingList(lat, lon, dist, rows,start, fullTextSearch, null);
    }
    
    
    @ApiOperation(value = "Get the parking with the given id")
    @ApiResponses(value = { 
	    @ApiResponse(code = 400, message = "Invalid request"),
	    @ApiResponse(code = 404, message = "Parking not found"),
	    @ApiResponse(code = 500, message = "Internal Server Error"),
	    @ApiResponse(code = 200, message = "The requested parking ") })
	@RequestMapping(value="/parkings/{parkingId}", method=RequestMethod.GET)
    public Parking  getParkingById (
	    	@ApiParam(name = "parkingId", value = "Unique id of the parking") 
	    	@PathVariable(value="parkingId", required=true) String parkingId) {
        
		logger.debug("GET parking by Id called with id={}", parkingId);
		 
		return parkingService.getParkingById(parkingId);
    }
}
