package com.instantsystem.exercice.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.naming.spi.DirStateFactory.Result;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.instantsystem.exercice.exception.ParkingException;
import com.instantsystem.exercice.exception.ParkingNotFoundException;
import com.instantsystem.exercice.model.Parking;

/**
 * This service class is in charge of processing all operations related to Parkings
 * @author fdiop
 *
 */
@Service
public class ParkingService {

    private static Logger LOG = LoggerFactory.getLogger(ParkingService.class);
	    
    @Autowired
    ParkingRestClient parkingRestClient;

    @Value("${recordsPropertyName}")
    private String recordsPropertydName;
    
    @Value("${parkingModelPropertydName}")
    private String parkingModelPropertydName;
    
    /**
     * Helper for json to POJO mapping
     */
    private Gson gson = new Gson();
    /**
     * Helper for json parsing
     */
    private JSONParser parser = new JSONParser();
    
    
    public List<Parking> getParkingList(Double lat, Double lon, Double dist, Integer rows, Integer start,
	    String fullTextSearch, String parkingId) {

	ResponseEntity<JSONObject> response = parkingRestClient.getParkingsRest(lat, lon, dist, rows, start, fullTextSearch, parkingId);
	LOG.debug("recieved response = {}", response);
	
	// we extract the received datas and map them to our internal model
	return response != null ? mapToParkingModel(response.getBody()) : Collections.emptyList();
    }

    /**
     * Extracts the body message and map it to our internal model
     * @param responseBody from the external resource
     * @return a list of our internal data model Parking
     */
    @SuppressWarnings("unchecked")
    private List<Parking> mapToParkingModel(JSONObject responseBody) {
	
	// extract the raw body
	JSONArray listRawRecords =  Optional.ofNullable(responseBody)
                            	.map(body -> gson.toJson(body.get(recordsPropertydName)))
                            	.map(arrayString ->  {
                            		    try {
                            			return (JSONArray)parser.parse(arrayString);
                            		    } catch (ParseException e) {
                            			LOG.warn("Error while parsing recieved datas", e);
                            			throw new ParkingException("Could not parse recieved datas");
                            		    }
                            		})
                            	.orElse(new JSONArray());
	
	
	
	LOG.debug("extracted body: listRawRecords = {}", listRawRecords);
	
	// map datas
	List<Parking> listParkings = (List<Parking>) listRawRecords.stream()
        	.map(rawRec -> ((JSONObject)rawRec).get(parkingModelPropertydName))
        	.map(rawParking -> gson.fromJson(rawParking.toString(), Parking.class))
        	.collect(Collectors.toList());
		
	LOG.debug("Number of mapped parking objects= {}", listParkings.size());	
	return listParkings;
    }

    /**
     * @param parkingId
     * @return the parking with the given parkingId
     * @throws ParkingNotFoundException
     */
    public Parking getParkingById(String parkingId) throws ParkingNotFoundException{
	List<Parking> result = getParkingList(null, null, null, null, null, null, parkingId);
	if (!result.isEmpty()) {
	    return result.get(0);
	} 
	throw new ParkingNotFoundException(String.format("Cannot find Parking with id=%s", parkingId));
    }

}
