package com.instantsystem.exercice.service;


import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.instantsystem.exercice.exception.InvalidCoordinateException;
import com.instantsystem.exercice.exception.InvalidDistanceException;
import com.instantsystem.exercice.exception.ParkingException;


/**
 * @author fdiop
 * Is in charge of invoking the external resource
 */
@Service
public class ParkingRestClient {

    private static Logger LOG = LoggerFactory.getLogger(ParkingRestClient.class);
	    
    @Autowired
    private RestTemplate restTemplate;

    @Value("${parkings.api.url}")
    private String parkingsApiUrl;

    
    /**
     * Fetch the parkings that match the given criterias 
     * @param lat
     * @param lon
     * @param dist
     * @param rows
     * @param start
     * @param fullTextSearch
     * @return
     */
    public  ResponseEntity<JSONObject> getParkingsRest(Double lat, Double lon, Double dist, Integer rows, 
	    						Integer start, String fullTextSearch, String parkingId) {
	StringBuilder url = new StringBuilder(parkingsApiUrl);
	
	try {
	    // build the query parameters
	    String distanceQueryParams = validateSearchPointAndDist(lat, lon, dist);
	    String additionalQueryParams = buildAdditionalQueryParams(rows, start, fullTextSearch, parkingId);
	    
	    // append the query parameters if any
	    url.append(distanceQueryParams);
	    url.append(additionalQueryParams);

	    return this.restTemplate.exchange(url.toString(), HttpMethod.GET,
		    HttpEntity.EMPTY, JSONObject.class);

	} catch (HttpClientErrorException ex) {
	    String message = String.format("Error while invoking external resource. url=%s", url);
	     LOG.error(message, ex);
	     throw new ParkingException(message);
	} catch (RestClientException ex) {
	    String message = String.format("Error while invoking external resource. url=%s", url);
	    LOG.error(message, ex);
	    throw new ParkingException(message);
	} 
    }

    
    /**
     * Build the additional query parameters that will be added to the request 
     * @param rows total number of rows to display
     * @param start index from which we start to display
     * @param fullTextSearch full text search inside the records
     * @return a String representing the additional query parameters that will be added to the request 
     */
    private String buildAdditionalQueryParams(Integer rows, Integer start, String fullTextSearch, String parkingId) {
	StringBuilder parameters = new StringBuilder();
	if (rows != null) {
	    parameters.append("&rows=").append(rows);
	}
	if (start != null) {
	    parameters.append("&start=").append(start);
	}
	if (StringUtils.isNotBlank(fullTextSearch)) {
	    parameters.append("&q=").append(fullTextSearch);
	}
	if (StringUtils.isNotBlank(parkingId)) {
	    parameters.append("&refine.id=").append(parkingId);
	}
	return parameters.toString();
    }

    /**
     * Validates the coordinates and the distance parameters
     * 
     * @param lat
     * @param lon
     * @param dist
     * @return a String object representing the query parameters to append to
     *         the request
     * @throws InvalidCoordinateException when the couple (lat, lon) s not valid
     * @throws InvalidDistanceException when the coordinates are valid and dist parameter is not provided
     * 		or negative
     */
    private String validateSearchPointAndDist(Double lat, Double lon, Double dist) throws InvalidCoordinateException, InvalidDistanceException{
	StringBuilder parameters = new StringBuilder();
	if (lat == null && lon == null) {
	    // no coordinate were given
	    if (dist != null) {
		// distance provided without coordinates
		throw new InvalidCoordinateException(
			    String.format("Distance should be associated to valid coordinates: dist=%s, lat=%s, lon=%s", dist, lat, lon));
	    }
	    // we perform the default query
	    return parameters.toString();
	} else if (lat == null || lon == null) {
	    // the given coordinates are not valid
	    // we throw an exception
	    throw new InvalidCoordinateException(
		    String.format("One of the given coordinates is null: lat=%s, lon=%s", lat, lon));
	} else if (dist == null || dist <= 0) {
	    // the given distance is not valid
	    // we throw an exception
	    throw new InvalidDistanceException(String.format("The distance is missing, negative or equals to zero: dist=%s", dist));
	}

	// everything is valid, then we can append the query parameters
	parameters.append("&").append("geofilter.distance=").append(lat).append(",").append(lon).append(",")
		.append(dist);

	return parameters.toString();
    }


}
