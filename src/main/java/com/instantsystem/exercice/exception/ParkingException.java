package com.instantsystem.exercice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author fdiop
 * Raised in a general purpose
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ParkingException extends RuntimeException {


    /**
     * 
     */
    private static final long serialVersionUID = -109640252566885353L;

    public ParkingException(String message) {
	super(message);
    }
}
