package com.instantsystem.exercice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author fdiop
 * Raised when the given coordinates are not valid
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidCoordinateException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -1171157750747237665L;

    public InvalidCoordinateException(String message) {
	super(message);
    }
}
