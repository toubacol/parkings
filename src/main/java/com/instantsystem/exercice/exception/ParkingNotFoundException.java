package com.instantsystem.exercice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author fdiop
 * Raised when the requested parking Id could not be found
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ParkingNotFoundException extends RuntimeException {


    /**
     * 
     */
    private static final long serialVersionUID = 8468072752714831512L;

    public ParkingNotFoundException(String message) {
	super(message);
    }
}
