package com.instantsystem.exercice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author fdiop
 * Raised when the given distance is not valid
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidDistanceException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 12892039146591093L;

    public InvalidDistanceException(String message) {
	super(message);
    }
}
